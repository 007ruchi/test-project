.PHONY: foo

foo:
	echo "All done!"
	for n in {1..1000000};  do  \
		echo "All clean!";  \
		echo "$n"; \
		echo THIS IS A TEST LOG LONG LINE THIS IS A TEST LOG LONG LINE THIS IS A TEST LOG LONG LINE THIS IS A TEST LOG LONG LINE THIS IS A TEST LOG LONG LINE THIS IS A TEST LOG LONG LINE THIS IS A TEST LOG LONG LINE; \
		sleep 0.01;         \
	done

.PHONY: all
all:
	echo "Executing all ..."
